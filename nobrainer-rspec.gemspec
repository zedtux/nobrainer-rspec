# frozen_string_literal: true

$LOAD_PATH.push File.expand_path('lib', __dir__)

require 'nobrainer/rspec/version'

Gem::Specification.new do |spec|
  spec.name        = 'nobrainer-rspec'
  spec.version     = NoBrainer::RSpec::VERSION
  spec.platform    = Gem::Platform::RUBY
  spec.authors     = ['Guillaume Hain']
  spec.email       = ['zedtux@zedroot.org']
  spec.homepage    = 'https://gitlab.com/zedtux/nobrainer-rspec'
  spec.summary     = 'RSpec matchers for Nobrainer'
  spec.description = 'RSpec matches for Nobrainer models, including ' \
                     'association and validation matchers.'
  spec.license     = 'MIT'

  spec.required_ruby_version = '>= 2.2'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'
  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = "#{spec.homepage}/-/blob/master/CHANGELOG.md"

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{
      ^(test|spec|features)/|
      \.gitlab-ci.yml|
      \.rspec|
      \.gitignore|
      Earthfile|
      Dockerfile|
      docker-compose.yml
    }x)
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'nobrainer'
  spec.add_dependency 'rspec-core', '~> 3.3'
  spec.add_dependency 'rspec-expectations', '~> 3.3'
  spec.add_dependency 'rspec-mocks', '~> 3.3'
  spec.add_development_dependency 'appraisal', '~> 2.2'
  spec.add_development_dependency 'rake', '~> 10.0'
end
