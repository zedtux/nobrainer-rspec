# nobrainer-rspec

The nobrainer-rspec library provides a collection of RSpec-compatible matchers that help to test NoBrainer documents.

This gem is heavily inspiring from the [mongoid-rspec](https://github.com/mongoid/mongoid-rspec) gem, keeping same API so that migrating from one to the other is easy.

## Installation

Drop this line into your Gemfile:

```ruby
group :test do
  gem 'nobrainer-rspec'
end

```

## Configuration

Add to your `rails_helper.rb` or `spec_helper.rb` file.

```ruby
require 'nobrainer-rspec'

RSpec.configure do |config|
  config.include NoBrainer::Matchers
end
```

## Matchers

* [be_nobrainer_document](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/be_nobrainer_document.rb)
* [have_field](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/have_field.rb)
* [have_index_for](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/have_index_for.rb)
* [have_timestamps](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/have_timestamps.rb)

## Associations

* [belong_to, have_one, have_many](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/associations.rb)

## Validations

* [validate_acceptance_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/acceptance_of.rb)
* [validate_confirmation_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/confirmation_of.rb)
* [validate_exclusion_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/exclusion_of.rb)
* [validate_format_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/format_of.rb)
* [validate_inclusion_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/inclusion_of.rb)
* [validate_length_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/length_of.rb)
* [validate_numericality_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/numericality_of.rb)
* [validate_presence_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/presence_of.rb)
* [validate_uniqueness_of](https://gitlab.com/zedtux/nobrainer-rspec/-/blob/master/lib/matchers/validations/uniqueness_of.rb)

## Contributing

You're encouraged to contribute to this library.

## Copyright and License

Copyright (c) 2020 Guillaume Hain and Contributors.

MIT License. See [LICENSE](LICENSE) for details.
