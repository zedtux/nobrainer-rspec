# frozen_string_literal: true

# Runs the `NoBrainer.configure` so that it assigns the default values and
# passing the `app_name` used in building the database name.
# See http://nobrainer.io/docs/installation/#configuring-nobrainer-manually
NoBrainer.configure do |config|
  # app_name is the name of your application in lowercase.
  # When using Rails, the application name is automatically inferred.
  config.app_name = 'rspec'

  # environment defaults to Rails.env for Rails apps or to the environment
  # variables RUBY_ENV, RAILS_ENV, RACK_ENV, or :production.
  config.environment = 'test'
end
