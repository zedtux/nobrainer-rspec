# frozen_string_literal: true

class User
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  field :email, type: String, format: /@/, index: true, required: true, default: '', uniq: { scope: :organisation_id }
  field :tags, index: :multi
  field :firstname
  field :lastname
  field :uuid, type: String, readonly: true, unique: true, primary_key: true
  field :password, type: String, length: (8..26)
  field :avatar, type: Binary, lazy_fetch: true
  field :age, type: Integer, required: true
  field :adult, type: Boolean, default: false
  field :bio, type: Text, max_length: 250
  field :rating_average, type: Float
  field :nickname, type: Symbol, min_length: 3
  field :status, type: Enum, in: %i[pending accepted rejected]
  field :birthday, type: Date, store_as: :bday
  field :hobbies, type: Array, default: []
  field :skills, type: Set
  field :settings, type: Hash, default: { theme: 'dark' }
  field :position, type: Geo::Point

  index %i[email nickname]
  index :full_name_compound, %i[firstname lastname]

  belongs_to :country, primary_key: :country_id
  belongs_to :organisation, class_name: 'Association'
  belongs_to :group, foreign_key: :member_id
  belongs_to :team, required: true
  has_one :address, class_name: 'User::Address'
  has_one :city, through: :address
  has_many :groups, dependent: :destroy
  has_many :friends, through: :groups

  validates_presence_of :firstname
  validates :email, presence: true, uniqueness: { scope: :organisation_id },
                                    format: { with: /@/ },
                                    exclusion: { in: %w[super index edit] },
                                    confirmation: true
  validates :terms_of_service, acceptance: true
  validates :age, presence: true, numericality: true, inclusion: { in: 23..42 },
                  on: %i[create update]
end
