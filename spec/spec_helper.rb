# frozen_string_literal: true

require 'bundler/setup'
require 'byebug'

require 'nobrainer'
# require 'rspec/core'
# require 'rspec/expectations'

Dir[
  File.join(File.expand_path(__dir__), 'support', '*.rb')
].sort.each { |file| require file }

Dir[
  File.join(File.expand_path(__dir__), 'models', '*.rb')
].sort.each { |file| require file }

require 'nobrainer-rspec'

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # These two settings work together to allow you to limit a spec run
  # to individual examples or groups you care about by tagging them with
  # `:focus` metadata. When nothing is tagged with `:focus`, all examples
  # get run.
  config.filter_run :focus
  config.run_all_when_everything_filtered = true

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.include RSpec::Matchers
  config.include NoBrainer::Matchers
end
