# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::HaveTimestamps do
  describe User do
    it { is_expected.to have_timestamps }
  end
end
