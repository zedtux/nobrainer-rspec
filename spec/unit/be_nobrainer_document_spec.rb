# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::BeNoBrainerDocument do
  context "when model does't include NoBrainer::Document" do
    subject do
      Class.new
    end

    it { is_expected.not_to be_nobrainer_document }
  end

  context 'when model does include NoBrainer::Document' do
    subject do
      Class.new do
        include NoBrainer::Document
      end
    end

    it { is_expected.to be_nobrainer_document }
  end
end
