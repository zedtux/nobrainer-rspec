# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::Validations::HaveValidationMatcher do
  describe User do
    it { is_expected.to validate_presence_of(:firstname) }
    it do
      is_expected.to validate_uniqueness_of(:email).scoped_to(:organisation_id)
    end
    it { is_expected.to validate_uniqueness_of(:uuid) }
    it { is_expected.to validate_length_of(:bio).with_maximum(250) }
    it { is_expected.to validate_length_of(:nickname).with_minimum(3) }
    it do
      is_expected
        .to validate_inclusion_of(:status)
        .to_allow(:pending, :accepted, :rejected)
    end
    it do
      is_expected.to validate_format_of(:email).to_allow('zedtux@rethinkdb.com')
                                               .not_to_allow('invalid email')
    end
    it do
      is_expected
        .to validate_exclusion_of(:email).to_not_allow('super', 'index', 'edit')
    end
    it { is_expected.to validate_acceptance_of(:terms_of_service) }
    it { is_expected.to validate_confirmation_of(:email) }
    it { is_expected.to validate_numericality_of(:age).on(:create, :update) }
  end
end
