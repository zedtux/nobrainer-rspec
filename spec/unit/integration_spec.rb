# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'integration' do
  describe User do
    it do
      is_expected
        .to have_field(:email).of_type(String)
        .with_default_value_of('')
        .required
        .unique(true)
    end

    it do
      is_expected
        .to have_field(:email).of_type(String)
        .with_default_value_of('')
        .required
        .unique(scope: :organisation_id)
    end
  end
end
