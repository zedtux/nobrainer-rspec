# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::HaveIndexFor do
  describe User do
    it { is_expected.to have_index_for(:email) }
    it { is_expected.to have_index_for(:tags).with_multi }
    it { is_expected.to have_index_for(%i[email nickname]) }
    it do
      is_expected
        .to have_index_for(%i[firstname lastname]).named(:full_name_compound)
    end
  end
end
