# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::Associations do
  describe User do
    it { is_expected.to belong_to(:country).with_primary_key(:country_id) }
    it { is_expected.to belong_to(:organisation).class_name('Association') }
    it { is_expected.to belong_to(:group).with_foreign_key(:member_id) }
    it { is_expected.to belong_to(:team).with_required(true) }
    it { is_expected.to have_one(:address).class_name('User::Address') }
    it { is_expected.to have_one(:city).through(:address) }
    it { is_expected.to have_many(:groups).with_dependent(:destroy) }
    it { is_expected.to have_many(:friends).through(:groups) }
  end
end
