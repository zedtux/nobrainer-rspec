# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NoBrainer::Matchers::HaveField do
  describe User do
    it { is_expected.to have_field(:email).with_format(/@/).indexed }
    it { is_expected.to have_fields(:firstname, :lastname) }
    it do
      is_expected.to have_field(:uuid).of_type(String)
                                      .readonly
                                      .unique
                                      .with_primary_key
    end
    it do
      is_expected.to have_field(:password).of_type(String).of_length((8..26))
    end
    it do
      is_expected.to have_field(:avatar).of_type(NoBrainer::Binary)
                                        .lazy_fetched
    end
    it { is_expected.to have_field(:age).of_type(Integer).required }
    it do
      is_expected.to have_field(:adult).of_type(NoBrainer::Boolean)
                                       .with_default_value_of(false)
    end
    it do
      is_expected.to have_field(:bio).of_type(NoBrainer::Text)
                                     .with_max_length(250)
    end
    it { is_expected.to have_field(:rating_average).of_type(Float) }
    it do
      is_expected.to have_field(:nickname).of_type(Symbol)
                                          .with_min_length(3)
    end
    it do
      is_expected.to have_field(:status).of_type(NoBrainer::Enum)
                                        .to_allow(%i[pending accepted rejected])
    end
    it { is_expected.to have_field(:created_at).of_type(Time) }
    it { is_expected.to have_field(:birthday).of_type(Date).with_alias(:bday) }
    it do
      is_expected.to have_field(:hobbies).of_type(NoBrainer::Array)
                                         .with_default_value_of([])
    end
    it { is_expected.to have_field(:skills).of_type(Set) }
    it do
      is_expected.to have_field(:settings).of_type(Hash)
                                          .with_default_value_of(theme: 'dark')
    end
    it { is_expected.to have_field(:position).of_type(NoBrainer::Geo::Point) }
  end
end
