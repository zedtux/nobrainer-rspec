# frozen_string_literal: true

module NoBrainer
  module RSpec
    VERSION = '1.1.2'
  end
end
