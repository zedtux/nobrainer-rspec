# frozen_string_literal: true

$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'nobrainer'
require 'rspec/core'
require 'rspec/expectations'
require 'rspec/mocks'

require 'matchers/associations'
require 'matchers/be_nobrainer_document'
require 'matchers/have_field'
require 'matchers/have_index_for'
require 'matchers/have_timestamps'
require 'matchers/validations'
require 'matchers/validations/acceptance_of'
require 'matchers/validations/confirmation_of'
require 'matchers/validations/exclusion_of'
require 'matchers/validations/format_of'
require 'matchers/validations/inclusion_of'
require 'matchers/validations/length_of'
require 'matchers/validations/numericality_of'
require 'matchers/validations/presence_of'
require 'matchers/validations/uniqueness_of'

module NoBrainer
  module Matchers
    include NoBrainer::Matchers::Associations
    include NoBrainer::Matchers::Validations
  end
end
