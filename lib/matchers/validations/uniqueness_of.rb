# frozen_string_literal: true

module NoBrainer
  module Matchers
    module Validations
      class ValidateUniquenessOfMatcher < HaveValidationMatcher
        def initialize(field)
          super(field, :uniqueness)
        end

        def scoped_to(*scope)
          @scope = [scope].flatten.map(&:to_sym)
          self
        end
        alias scoped_on scoped_to

        def matches?(actual)
          return false unless @result = super(actual)

          check_scope if @scope

          @result
        end

        def description
          options_desc = []
          options_desc << " scoped to #{@scope.inspect}" if @scope
          "#{super}#{options_desc.to_sentence}"
        end

        private

        def check_scope
          message = " scope to #{@validator.options[:scope]}"

          if @validator.options[:scope]
            if [@validator.options[:scope] || ''].flatten.map(&:to_sym) == @scope
              @positive_result_message += message
            else
              @negative_result_message += message
            end
          else
            @negative_result_message += ' without a scope'
            @result = false
          end
        end
      end

      def validate_uniqueness_of(field)
        ValidateUniquenessOfMatcher.new(field)
      end
    end
  end
end
