# frozen_string_literal: true

module NoBrainer
  module Matchers
    def have_timestamps
      HaveTimestamps.new
    end

    class HaveTimestamps
      def initialize
        @root_module = 'NoBrainer::Document::Timestamps'
      end

      def matches?(actual)
        @model = actual.is_a?(Class) ? actual : actual.class
        @model.included_modules.include?(expected_module)
      end

      def description
        'be a NoBrainer document with timestamps'
      end

      def failure_message
        "Expected #{@model.inspect} class to #{description}"
      end

      def failure_message_when_negated
        "Expected #{@model.inspect} class to not #{description}"
      end

      private

      def expected_module
        @root_module.constantize
      end
    end
  end
end
