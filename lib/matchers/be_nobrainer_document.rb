# frozen_string_literal: true

module NoBrainer
  module Matchers
    def be_nobrainer_document
      BeNoBrainerDocument.new
    end

    class BeNoBrainerDocument
      def matches?(actual)
        @model = actual.is_a?(Class) ? actual : actual.class
        @model.included_modules.include?(NoBrainer::Document)
      end

      def description
        'include NoBrainer::Document'
      end

      def failure_message
        "expect #{@model.inspect} class to #{description}"
      end

      def failure_message_when_negated
        "expect #{@model.inspect} class to not #{description}"
      end
    end
  end
end
