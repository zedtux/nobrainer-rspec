# frozen_string_literal: true

module NoBrainer
  module Matchers
    # The `have_index_for` matcher tests that the table that backs your model
    # has a specific index.
    #
    #     class User
    #       include NoBrainer::Document
    #
    #       field :name, index: true
    #       field :lastname
    #
    #       index :lastname
    #     end
    #
    #     # RSpec
    #     RSpec.describe User, type: :model do
    #       it { is_expected.to have_index_for(:name) }
    #       it { is_expected.to have_index_for(:lastname) }
    #     end
    #
    # #### Qualifiers
    #
    # ##### with_multi
    #
    # Use `with_multi` to assert that an index is defined as multi for a certain
    # field.
    #
    #     class User
    #       include NoBrainer::Document
    #
    #       field :name, index: :multi
    #       field :lastname
    #
    #       index :lastname, multi: true
    #     end
    #
    #     # RSpec
    #     RSpec.describe User, type: :model do
    #       it do
    #         it { is_expected.to have_index_for(:name).with_multi }
    #         it { is_expected.to have_index_for(:lastname).with_multi }
    #       end
    #     end
    #
    # ##### named
    #
    # Use `named` to assert that an index has a certain name.
    #
    #     class User
    #       include NoBrainer::Document
    #
    #       field :firstname
    #       field :lastname
    #
    #       index :full_name_compound, %i[firstname lastname]
    #     end
    #
    #     # RSpec
    #     RSpec.describe User, type: :model do
    #       it do
    #         is_expected.to have_index_for(%i[firstname lastname])
    #           .named(:full_name_compound)
    #       end
    #     end
    #
    class HaveIndexFor # :nodoc:
      def initialize(*attrs)
        @attributes = attrs.collect do |attributes|
          if attributes.is_a?(Enumerable)
            attributes.collect(&:to_sym)
          else
            attributes.to_sym
          end
        end
      end

      def with_multi(multi = true)
        @multi = multi
        self
      end

      def named(index_name)
        @index_name = index_name
        self
      end

      def matches?(klass)
        @klass = klass.is_a?(Class) ? klass : klass.class
        @errors = []
        @attributes.each do |attr|
          if attr.is_a?(Enumerable)
            missing_fields = attr - @klass.fields.keys
            if missing_fields.empty?
              index_key = @index_name || attr.join('_').to_sym
              if @klass.indexes.include?(index_key)
                error = ''

                # Checking multi
                if @multi && (@klass.indexes[index_key][:multi] != @multi)
                  error += " with multi #{@klass.indexes[index_key][:multi].inspect}"
                end

                @errors.push("field #{attr.inspect}" + error) unless error.blank?
              else
                if @index_name
                  @errors.push "no compound index named #{@index_name}"
                else
                  @errors.push "no compound index for fields #{attr.to_sentence}"
                end
              end
            else
              @errors.push "the field#{'s' if missing_fields.size > 1} " \
                           "#{missing_fields.to_sentence} " \
                           "#{missing_fields.size > 1 ? 'are' : 'is'} missing"
            end
          else
            if @klass.fields.include?(attr)
              if @klass.indexes.include?(attr)
                error = ''

                # Checking multi
                if @multi && (@klass.indexes[attr][:multi] != @multi)
                  error += " with multi #{@klass.indexes[attr][:multi].inspect}"
                end

                @errors.push("field #{attr.inspect}" + error) unless error.blank?

              else
                @errors.push "no index for field #{attr.inspect}"
              end
            else
              @errors.push "no field named #{attr.inspect}"
            end
          end
        end
        @errors.empty?
      end

      def failure_message_for_should
        "Expected #{@klass.inspect} to #{description}, got #{@errors.to_sentence}"
      end

      def failure_message_for_should_not
        "Expected #{@klass.inspect} to not #{description}, got #{@klass.inspect} to #{description}"
      end

      alias failure_message failure_message_for_should
      alias failure_message_when_negated failure_message_for_should_not

      def description
        desc = "have index for #{@attributes.collect(&:inspect).to_sentence}"
        desc += ' to be multi' if @multi
        desc += " named #{@index_name}" if @index_name
        desc
      end
    end

    def have_index_for(*args)
      HaveIndexFor.new(*args)
    end
  end
end
