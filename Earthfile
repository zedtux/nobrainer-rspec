VERSION 0.6

FROM ruby:2.7-slim
WORKDIR /gem

deps:
    COPY Gemfile* /gem/
    COPY *.gemspec /gem
    COPY lib/nobrainer/rspec/version.rb /gem/lib/nobrainer/rspec/

    RUN apt update \
        && apt install --yes \
                       --no-install-recommends \
                       build-essential \
                       git \
        && bundle install --jobs $(nproc)

    SAVE ARTIFACT /usr/local/bundle bundler

dev:
    RUN apt update \
        && apt install --yes \
                       --no-install-recommends \
                       git

    COPY +deps/bundler /usr/local/bundle

    COPY *.gemspec /gem
    COPY bin/ /gem/bin/
    COPY .rspec /gem
    COPY Rakefile /gem

    COPY Gemfile* /gem

    COPY lib/ /gem/lib/
    COPY spec/ /gem/spec/

    ENTRYPOINT ["bundle", "exec"]
    CMD ["rake"]

    SAVE IMAGE --push registry.gitlab.com/zedtux/nobrainer-rspec:latest

rspec:
    FROM docker:19.03.13-dind

    RUN apk --update --no-cache add docker-compose

    COPY docker-compose.yml ./

    RUN pwd

    WITH DOCKER --load registry.gitlab.com/zedtux/nobrainer-rspec:latest=+dev \
                --pull rethinkdb:2.4-buster-slim
        RUN pwd \
            && docker-compose up -d \
            && docker run --network=host \
                          --entrypoint= \
                          zedtux/nobrainer-rspec:latest \
                          sh -c 'bundle && bundle exec rake' \
            && docker-compose down
    END

#
# This target is used to publish this gem to rubygems.org.
#
# Prerequiries
# You should have login again the Rubygems.org so that it has created
# the `~/.gem` folder and stored your API key.
#
# Then use the following command:
# earthly +gem --GEM_CREDENTIALS="$(cat ~/.gem/credentials)"
gem:
    FROM +dev

    ARG GEM_CREDENTIALS

    COPY .git/ /gem/
    COPY CHANGELOG.md /gem/
    COPY LICENSE /gem/
    COPY README.md /gem/

    RUN gem build nobrainer-rspec.gemspec \
        && mkdir ~/.gem \
        && echo "$GEM_CREDENTIALS" > ~/.gem/credentials \
        && cat ~/.gem/credentials \
        && chmod 600 ~/.gem/credentials \
        && gem push nobrainer-rspec-*.gem

    SAVE ARTIFACT nobrainer-rspec-*.gem AS LOCAL nobrainer-rspec.gem
