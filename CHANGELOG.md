# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.1.2] - 2022-05-09
### Fixed
- have_index_for with a coupound index

## [1.1.1] - 2021-02-09
### Fixed
- error on generating failure description #1

## [1.1.0] - 2020-11-28
### Fixed
- have_field with a field using `unique: true` or `uniq: true`

## [1.0.1] - 2020-11-27
### Fixed
- Fixes gem build which was creating an empty gem

## [1.0.0] - 2020-11-26
### Added
- be_nobrainer_document
- have_field
- have_index_for
- have_timestamps
- belong_to
- have_one
- have_one(...).through(...)
- have_many
- have_many(...).through(...)
- validate_presence_of
- validate_uniqueness_of
- validate_length_of
- validate_inclusion_of
- validate_acceptance_of
- validate_confirmation_of
- validate_exclusion_of
- validate_format_of
- validate_numericality_of

[Unreleased]: https://gitlab.com/zedtux/nobrainer-rspec/-/compare/v1.1.2...master
[1.1.2]: https://gitlab.com/zedtux/nobrainer-rspec/-/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/zedtux/nobrainer-rspec/-/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/zedtux/nobrainer-rspec/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/zedtux/nobrainer-rspec/-/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/zedtux/nobrainer-rspec/-/tags/v1.0.0
